//
// Created by lra on 22/11/22.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

static long cont = 0;

void print (const int *vec, const unsigned int ini, const unsigned int fim)
{
    
    for (int i = ini; i < fim; i++)
        printf ("%d  ", vec[i]);
    
    printf ("\n\n");
}

void insertsort (int *vec, const int ini, const int fim)
{
    int idx, val;
    for (int i = (ini + 1); i < fim; i++)
    {
        val = vec[i];
        idx = i - 1;
        
        if (idx >= 0 && vec[idx] > val)
            while (idx >= ini && vec[idx] > val)
            {
                vec[idx + 1] = vec[idx];
                idx--;
                cont++;
            }
        else
            cont++;
        
        vec[idx + 1] = val;
    }
}

void mesclar (int *vec, const int ini, const int avg, const int fim)
{
    
    int *vax = (int *) malloc ((fim - ini) * sizeof (int));
    
    int idxa = 0, idxi = ini, idxm = avg;
    
    while (idxi < avg && idxm < fim)
    {
        if (vec[idxi] < vec[idxm])
            vax[idxa++] = vec[idxi++];
        else
            vax[idxa++] = vec[idxm++];
        
        cont++;
    }
    while (idxi < avg)
    {
        vax[idxa++] = vec[idxi++];
        cont++;
    }
    
    while (idxm < fim)
    {
        vax[idxa++] = vec[idxm++];
        
        cont++;
    }
    
    for (int i = ini; i < fim; i++)
    {
        vec[i] = vax[(i - ini)];
        cont++;
    }
    free (vax);
}

void mergesort (int *vec, const int ini, const int fim)
{
    
    if ((fim - ini) <= 1)
        return;
    
    unsigned int avg = ini + ((fim - ini) / 2);
    
    mergesort (vec, ini, avg);
    
    mergesort (vec, avg, fim);
    
    mesclar (vec, ini, avg, fim);
    
}

void merginsort (int *vec, const int ksz, const int ini, const int fim)
{
    
    // print (vec, ini, fim);
    
    unsigned int sze = (fim - ini);
    
    if (sze <= 1)
        return;
    
    unsigned int avg = ini + (sze / 2);
    
    if (sze >= ksz)
    {
        merginsort (vec, ksz, ini, avg);
        merginsort (vec, ksz, avg, fim);
    }
    else
    {
        insertsort (vec, ini, fim);
        // print (vec, ini, fim);
    }
    
    mesclar (vec, ini, avg, fim);
    // print (vec, ini, fim);
}

int *create (int *vec, const unsigned int size)
{
    
    srand (43);
    
    for (int i = 0; i < size; i++)
        vec[i] = rand () % size;
    
    return vec;
}

int main (void)
{
    unsigned int size = 512;
    clock_t begin, end;
    
    int *vec = (int *) malloc ((size + 1) * sizeof (int));
    
    vec = create (vec, size);
    print (vec, 0, size);
    
    cont = 0;
    begin = clock ();
    mergesort (vec, 0, size);
    end = clock ();
    print (vec, 0, size);
    printf ("ms: %ld  %f\n\n", cont, ((double) (end - begin) / CLOCKS_PER_SEC));
    
    vec = create (vec, size);
    cont = 0;
    begin = clock ();
    insertsort (vec, 0, size);
    end = clock ();
    print (vec, 0, size);
    printf ("is: %ld  %f\n\n", cont, ((double) (end - begin) / CLOCKS_PER_SEC));
    
    for (int ksz = 4; ksz <= 128; ksz += 4)
    {
        vec = create (vec, size);
        cont = 0;
        begin = clock ();
        merginsort (vec, ksz, 0, size);
        end = clock ();
        print (vec, 0, size);
        printf ("mi: %2d  %ld  %f\n\n", ksz, cont, ((double) (end - begin) / CLOCKS_PER_SEC));
    }
    
    free (vec);
    
    printf ("\nFim!\n");
    return EXIT_SUCCESS;
}