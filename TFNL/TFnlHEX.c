//
// Created by lra on 04/11/22.
//

#include "include/L3GrafoMA.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum {
    PLY_NULL = 0,
    PLY_P1 = 1,
    PLY_P2 = 2
} player;

typedef struct board {
    
    unsigned int *left;
    unsigned int *right;
    
    unsigned int *top;
    unsigned int *down;
    
    unsigned int size;
    
    GMA *gma;
    
} BOARD;

double max (double m, double n)
{
    return (m >= n) ? m : n;
}

double min (double m, double n)
{
    return (m < n) ? m : n;
}

BOARD *tfn_add_tile (BOARD *board, const unsigned int v, const unsigned int tile)
{
    
    if (board == NULL || board->gma == NULL || v >= board->gma->num_vertex)
        return NULL;
    
    if (board->gma->vertex[v].tile == PLY_NULL)
        board->gma->vertex[v].tile = tile;
    
    return board;
}

// O(n²)
BOARD *tfh_create_rhombus_board (const unsigned int size)
{
    
    BOARD *board = (BOARD *) malloc (sizeof (BOARD));
    
    if (board == NULL)
        return NULL;
    
    board->gma = gma_create (pow (size, 2));
    
    if (board->gma == NULL)
        return NULL;
    
    board->size = size;
    
    board->left = (unsigned int *) malloc (size * sizeof (unsigned int));
    board->right = (unsigned int *) malloc (size * sizeof (unsigned int));
    
    board->top = (unsigned int *) malloc (size * sizeof (unsigned int));
    board->down = (unsigned int *) malloc (size * sizeof (unsigned int));
    
    board->gma->num_neighbors = 6;
    
    int mod, div;
    int l = 0, r = 0, t = 0, d = 0;
    for (int i = 0; i < board->gma->num_vertex; i++)
    {
        
        board->gma->vertex[i].neighbors = (int *) malloc (board->gma->num_neighbors * sizeof (int));
        for (int j = 0; j < board->gma->num_neighbors; j++)
            board->gma->vertex[i].neighbors[j] = -1;
        
        mod = board->gma->vertex[i].id % size;
        div = board->gma->vertex[i].id / size;
        
        if (mod == 0)
        {
            board->left[l++] = board->gma->vertex[i].id;
        }
        else if (mod == (size - 1))
        {
            board->right[r++] = board->gma->vertex[i].id;
        }
        
        if (div == 0)
        {
            board->top[t++] = board->gma->vertex[i].id;
        }
        else if (div == (size - 1))
        {
            board->down[d++] = board->gma->vertex[i].id;
        }
    }
    
    for (int i = 0; i < size; i++)
    {
        
        for (int j = 0; j < size; j++)
        {
            
            if ((i - 1) >= 0)
            {
                board->gma = gma_add_arc (board->gma, (i * size) + j, ((i - 1) * size) + j, 1);
                
                if ((j + 1) < size)
                    board->gma = gma_add_arc (board->gma, (i * size) + j, ((i - 1) * size) + (j + 1), 1);
            }
            
            if ((j + 1) < size)
                board->gma = gma_add_arc (board->gma, (i * size) + j, (i * size) + (j + 1), 1);
            
            if ((i + 1) < size)
            {
                board->gma = gma_add_arc (board->gma, (i * size) + j, ((i + 1) * size) + j, 1);
                
                if ((j - 1) >= 0)
                    board->gma = gma_add_arc (board->gma, (i * size) + j, ((i + 1) * size) + (j - 1), 1);
            }
            
            if ((j - 1) >= 0)
                board->gma = gma_add_arc (board->gma, (i * size) + j, (i * size) + (j - 1), 1);
        }
    }
    
    return board;
}

BOARD *tfh_clone_board (BOARD *board)
{
    
    BOARD *nboard = (BOARD *) malloc (sizeof (BOARD));
    
    if (nboard == NULL)
        return NULL;
    
    nboard->size = board->size;
    
    nboard->gma = gma_create (pow (nboard->size, 2));
    
    if (nboard->gma == NULL)
        return NULL;
    
    nboard->left = (unsigned int *) malloc (nboard->size * sizeof (unsigned int));
    nboard->right = (unsigned int *) malloc (nboard->size * sizeof (unsigned int));
    nboard->top = (unsigned int *) malloc (nboard->size * sizeof (unsigned int));
    nboard->down = (unsigned int *) malloc (nboard->size * sizeof (unsigned int));
    
    for (int j = 0; j < nboard->size; j++)
    {
        nboard->left[j] = board->left[j];
        nboard->right[j] = board->right[j];
        nboard->top[j] = board->top[j];
        nboard->down[j] = board->down[j];
    }
    
    nboard->gma->num_neighbors = board->gma->num_neighbors;
    
    for (int i = 0; i < nboard->gma->num_vertex; i++)
    {
    
        nboard->gma->vertex[i].id = board->gma->vertex[i].id;
        nboard->gma->vertex[i].tile = board->gma->vertex[i].tile;
        nboard->gma->vertex[i].num_neighbors = board->gma->vertex[i].num_neighbors;
    
        nboard->gma->vertex[i].neighbors = (int *) malloc (nboard->gma->num_neighbors * sizeof (int));
    
        for (int j = 0; j < nboard->gma->num_neighbors; j++)
            nboard->gma->vertex[i].neighbors[j] = board->gma->vertex[i].neighbors[j];
    
        for (int j = 0; j < nboard->gma->num_vertex; j++)
        {
            nboard->gma->arcs[i][j].link = board->gma->arcs[i][j].link;
            nboard->gma->arcs[i][j].cost = board->gma->arcs[i][j].cost;
        }
    }
    
    return nboard;
}

void tfn_destroy_board (BOARD *board)
{
    
    gma_destroy (board->gma);
    
    free (board->left);
    free (board->right);
    free (board->top);
    free (board->down);
    free (board);
    
    board = NULL;
}

double tfn_astarh_manhattan (const GMA *gma, const unsigned int v1, const unsigned int v2)
{
    int rq = sqrt (gma->num_vertex);
    
    int v1x = (v1 / rq);
    int v1y = (v1 % rq);
    
    int v2x = (v2 / rq);
    int v2y = (v2 % rq);
    
    int dx = abs (v1x - v2x);
    int dy = abs (v1y - v2y);
    
    return (dx + dy);
}

int
tfn_spf_astar (GMA *gma, player player, double (*heuristic) (const GMA *, const unsigned int, const unsigned int), const unsigned int v1, const unsigned int v2, int *path, unsigned int *dist)
{
    
    if (gma == NULL)
        return 0;
    
    if (!(gma->vertex[v1].tile == 0 || gma->vertex[v1].tile == player))
        return 0;
    
    if (!(gma->vertex[v2].tile == 0 || gma->vertex[v2].tile == player))
        return 0;
    
    if (gma->vertex[v1].id == gma->vertex[v2].id)
        return 0;
    
    int *visited = (int *) malloc (gma->num_vertex * sizeof (int));
    
    double *prrt = (double *) malloc (gma->num_vertex * sizeof (double));
    
    int i;
    
    for (i = 0; i < gma->num_vertex; i++)
    {
        visited[i] = 0;
        dist[i] = INT_MAX;
        prrt[i] = INT_MAX;
        path[i] = -1;
    }
    
    path[v1] = v1;
    dist[v1] = 1;
    
    prrt[v1] = (*heuristic) (gma, v1, v2);
    
    int ret = 1;
    while (1)
    {
    
        double min = INT_MAX;
        int idx = -1;
    
        for (i = 0; i < gma->num_vertex; i++)
        {
            if (prrt[i] < min && !visited[i])
            {
                min = prrt[i];
                idx = i;
            }
        }
    
        if (idx == -1 || min == INT_MAX)
        {
            ret = 0;
            break;
        }
    
        if (idx == v2)
            break;
    
        if (gma->vertex[idx].tile == 0 || gma->vertex[idx].tile == player)
        {
    
            for (int j = 0; j < gma->vertex[idx].num_neighbors; j++)
            {
                i = gma->vertex[idx].neighbors[j];
    
                if (gma->arcs[idx][i].link == 1 && visited[i] == 0
                    && (gma->vertex[i].tile == 0 || gma->vertex[i].tile == player))
                {
                    if (dist[idx] + gma->arcs[idx][i].cost < dist[i])
                    {
                        dist[i] = dist[idx] + gma->arcs[idx][i].cost;
                        prrt[i] = dist[i] + (*heuristic) (gma, i, v2);
                        path[i] = idx;
                    }
                }
            }
        }
    
        visited[idx] = 1;
    }
    
    free (prrt);
    free (visited);
    
    return ret;
}

void
tfn_dfsr (const GMA *gma, player player, const unsigned int ini, const unsigned int fin, int *ret, int *ctrl)
{
    if (*ret >= 0)
        return;
    
    ctrl[ini] = 1;
    
    for (int i, j = 0; j < gma->vertex[ini].num_neighbors; j++)
    {
        i = gma->vertex[ini].neighbors[j];
        if (gma->arcs[ini][i].link == 1 && ctrl[i] == -1 && gma->vertex[i].tile == player)
        {
            if (i == fin)
            {
                *ret = i;
                break;
            }
            tfn_dfsr (gma, player, i, fin, ret, ctrl);
        }
    }
    
}

int
tfn_verify_finish (const BOARD *board, player player)
{
    
    if (board == NULL || board->gma == NULL)
        return 0;
    
    GMA *gma = board->gma;
    
    int *visited = (int *) malloc (gma->num_vertex * sizeof (int));
    
    int ini, fin, ret = -1;
    for (int i = 0; i < board->size; i++)
    {
        
        if (player == PLY_P1)
            fin = board->right[i];
        else if (player == PLY_P2)
            fin = board->down[i];
    
        if (gma->vertex[fin].tile != player)
            continue;
        
        for (int j = 0; j < board->size; j++)
        {
    
            if (player == PLY_P1)
                ini = board->left[j];
            else if (player == PLY_P2)
                ini = board->top[j];
    
            if (gma->vertex[ini].tile != player)
                continue;
    
            for (int l = 0; l < gma->num_vertex; l++)
                visited[l] = -1;
    
            tfn_dfsr (gma, player, ini, fin, &ret, visited);
    
            if (ret >= 0)
                break;
    
        }
        
        if (ret >= 0)
            break;
        
    }
    
    free (visited);
    
    return ret;
}

void
tfh_spf (const BOARD *board, player player, int *path, unsigned int *dist, int *min, int *stt, int *stp)
{
    if (board == NULL || board->gma == NULL)
        return;
    
    int *tpath = (int *) malloc (board->gma->num_vertex * sizeof (int));
    unsigned int *tdist = (unsigned int *) malloc (board->gma->num_vertex * sizeof (unsigned int));
    
    *min = INT_MAX;
    int ret = 0, ini = -1, fin = -1, dst = -1;
    for (int i = 0; i < board->size; i++)
    {
        
        if (player == PLY_P1)
            fin = board->right[i];
        else if (player == PLY_P2)
            fin = board->down[i];
        
        if (!(board->gma->vertex[fin].tile == 0 || board->gma->vertex[fin].tile == player))
            continue;
        
        for (int j = 0; j < board->size; j++)
        {
            
            if (player == PLY_P1)
                ini = board->left[j];
            else if (player == PLY_P2)
                ini = board->top[j];
            
            if (!(board->gma->vertex[ini].tile == 0 || board->gma->vertex[ini].tile == player))
                continue;
            
            if (board->gma->vertex[ini].id == board->gma->vertex[fin].id)
                continue;
            
            ret = tfn_spf_astar (board->gma, player, tfn_astarh_manhattan, ini, fin, tpath, tdist);
            
            if (ret == 0)
                continue;
            
            dst = tdist[fin];
            
            if (dst < 0 || dst >= board->gma->num_vertex)
                continue;
            
            for (int k = 0, mov = fin; k < tdist[fin]; k++)
            {
                if (board->gma->vertex[mov].tile == player)
                    dst--;
                
                mov = tpath[mov];
            }
            
            if (dst < *min)
            {
                *stt = ini;
                *stp = fin;
                *min = dst;
                
                for (int l = 0; l < board->gma->num_vertex; l++)
                {
                    path[l] = tpath[l];
                    dist[l] = tdist[l];
                }
            }
        }
    }
    
    free (tpath);
    free (tdist);
}

double tfn_heuristic_score (const BOARD *board, player ply_p1, player ply_p2)
{
    int stt_p1, stp_p1, min_p1, stt_p2, stp_p2, min_p2;
    
    stt_p1 = stp_p1 = min_p1 = stt_p2 = stp_p2 = min_p2 = -1;
    
    if (tfn_verify_finish (board, ply_p1) > 0)
        return INT_MIN;
    
    if (tfn_verify_finish (board, ply_p2) > 0)
        return INT_MAX;
    
    {
        
        {
            int *path_p1 = (int *) malloc (board->gma->num_vertex * sizeof (int));
            unsigned int *dist_p1 = (unsigned int *) malloc (board->gma->num_vertex * sizeof (unsigned int));
            
            tfh_spf (board, ply_p1, path_p1, dist_p1, &min_p1, &stt_p1, &stp_p1);
            
            free (path_p1);
            free (dist_p1);
        }
        
        {
            int *path_p2 = (int *) malloc (board->gma->num_vertex * sizeof (int));
            unsigned int *dist_p2 = (unsigned int *) malloc (board->gma->num_vertex * sizeof (unsigned int));
            
            tfh_spf (board, ply_p2, path_p2, dist_p2, &min_p2, &stt_p2, &stp_p2);
            
            free (path_p2);
            free (dist_p2);
        }
        
    }
    
    return (min_p1 - min_p2);
}

// O(n)
unsigned int tfn_is_full (GMA *gma)
{
    
    int i, free = 0;
    
    for (i = 0; i < gma->num_vertex; i++)
        if (gma->vertex[i].tile != 0)
        {
            free++;
            break;
        }
    
    return (free > 0 ? 0 : 1);
}

int *tfn_best_moves (BOARD *board, player plyr, unsigned int *size)
{
    
    int stt = -1, stp = -1, min = INT_MAX;
    
    int *path = (int *) malloc (board->gma->num_vertex * sizeof (int));
    unsigned int *dist = (unsigned int *) malloc (board->gma->num_vertex * sizeof (unsigned int));
    
    tfh_spf (board, plyr, path, dist, &min, &stt, &stp);
    
    int *moves = NULL;
    
    if (min < INT_MAX)
    {
        *size = dist[stp];
        
        if (*size > 0 && *size < board->gma->num_vertex)
        {
            
            moves = (int *) malloc (*size * sizeof (int));
    
            for (int pth = stp, i = *size - 1; i >= 0; i--)
            {
                moves[i] = pth;
                pth = path[pth];
            }
            
        }
    }
    free (path);
    free (dist);
    
    return moves;
}

double
tfn_minmax (BOARD *board, player ply_turn, player ply_next, const unsigned int maximizingPlayer, int depth, int *move, int *alpha, int *beta)
{
    
    unsigned int is_full = tfn_is_full (board->gma);
    
    if ((depth <= 0) || (is_full))
    {
        return tfn_heuristic_score (board, ply_turn, ply_next);
    }
    
    if (!is_full)
    {
    
        int *moves = NULL, mv = -1;
        unsigned int size = 0;
    
        if (maximizingPlayer)
        {
            double value = INT_MIN;
    
            moves = tfn_best_moves (board, ply_turn, &size);
    
            if (moves == NULL)
                size = board->gma->num_vertex;
    
            int m, d = 0;
            for (int i = 0; i < size; i++)
            {
    
                m = moves != NULL ? moves[i] : i;
    
                if (board->gma->vertex[m].tile != PLY_NULL)
                    continue;
    
                BOARD *updatedBoard = tfh_clone_board (board);
                updatedBoard = tfn_add_tile (updatedBoard, m, ply_turn);
    
                value = max (value, tfn_minmax (updatedBoard, ply_turn, ply_next, 0, (depth - 1), &mv, alpha, beta));
    
                tfn_destroy_board (updatedBoard);
    
                *alpha = max (*alpha, value);
    
                if (value >= *beta)
                {
                    if (move != NULL)
                        *move = m;
                }
    
                if ((*move > -1) && (++d > board->size))
                    break;
    
            }
    
            if (moves)
                free (moves);
    
            return value;
        }
        else
        {
    
            double value = INT_MAX;
    
            moves = tfn_best_moves (board, ply_next, &size);
    
            if (moves == NULL)
                size = board->gma->num_vertex;
    
            int m, d = 0;
            for (int i = 0; i < size; i++)
            {
    
                m = moves != NULL ? moves[i] : i;
    
                if (board->gma->vertex[m].tile != PLY_NULL)
                    continue;
    
                BOARD *updatedBoard = tfh_clone_board (board);
                updatedBoard = tfn_add_tile (updatedBoard, m, ply_next);
    
                value = min (value, tfn_minmax (updatedBoard, ply_turn, ply_next, 1, (depth - 1), &mv, alpha, beta));
    
                tfn_destroy_board (updatedBoard);
    
                *beta = min (*beta, value);
    
                if (value <= *alpha)
                {
                    if (move != NULL)
                        *move = m;
                }
    
                if ((*move > -1) && (++d > board->size))
                    break;
    
            }
    
            if (moves)
                free (moves);
    
            return value;
        }
    
    }
    else
    {
        return tfn_heuristic_score (board, ply_turn, ply_next);
    }
}

int tfn_hex (int n, int dap, int dad)
{
    
    // int dap = atol (argv[2]);
    // int dad = atol (argv[3]);
    // BOARD *board = tfh_create_rhombus_board (atol (argv[1]));
    
    BOARD *board = tfh_create_rhombus_board (n);
    
    int move, ret, alpha, beta;
    
    double abpmm = INT_MIN, ret_p1 = INT_MIN, max_p1 = 1, ret_p2 = INT_MIN, max_p2 = 0, md_p1 = 0, md_p2 = 0;
    
    gma_print (board->gma);
    
    while (1)
    {
    
        alpha = INT_MAX, beta = INT_MIN;
        abpmm = tfn_minmax (board, PLY_P1, PLY_P2, max_p1, dap, &move, &alpha, &beta);
        tfn_add_tile (board, move, PLY_P1);
        
        printf ("\nJogada: %d %d", PLY_P1, move);
        
        move = -1;
        if ((ret = tfn_verify_finish (board, PLY_P1)) >= 0)
        {
            printf ("\n\nFim de Jogo - P1 Ganhou! \n");
            break;
        }
    
        alpha = INT_MAX, beta = INT_MIN;
        abpmm = tfn_minmax (board, PLY_P2, PLY_P1, max_p2, dad, &move, &alpha, &beta);
        tfn_add_tile (board, move, PLY_P2);
        
        printf ("\nJogada: %d %d", PLY_P2, move);
        move = -1;
        
        if ((ret = tfn_verify_finish (board, PLY_P2)) >= 0)
        {
            printf ("\n\nFim de Jogo - P2 Ganhou! \n");
            break;
        }
    
    }
    
    gma_print (board->gma);
    
    gma_destroy (board->gma);
    
    printf ("\nFim!\n");
    
    return EXIT_SUCCESS;
}

int main (int argc, char **argv)
{
    // Teste 01
    // return tfn_hex (9, 2, 6);
    
    // Teste 02
    return tfn_hex (5, 2, INT_MAX);
    
    // Teste 03
    // return tfn_hex (496, 2, 6);
    
    // Teste 04
    // return tfn_hex (11, 2, 10);
}
