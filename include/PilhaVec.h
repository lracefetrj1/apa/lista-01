#ifndef _PILHAVEC_H_
#define _PILHAVEC_H_
#include <stdlib.h>

/* TAD: pilha de valores reais (float) */
typedef struct pilha Pilha;
typedef struct elemento Elemento;

int vplh_esta_vazia (Pilha *p);

/* nó da lista para armazenar valores reais */
struct elemento {
    int info;
    int prox;
    int pos;
    int uso;
};

/* estrutura da pilha */
struct pilha {
    
    Elemento *vec;
    int tam;
    
    int prim; /* aponta para o topo da pilha */
};

int vplh_obter_memoria_livre (Pilha *plh)
{
    
    if (plh == NULL)
        return 0;
    
    int pos = -1;
    
    for (int i = 0; i < plh->tam; i++)
    {
        if (plh->vec[i].uso == 0)
        {
            pos = i;
            break;
        }
    }
    
    return pos;
}

Pilha *vplh_criar (Elemento *vec, int tam)
{
    Pilha *plh = (Pilha *) malloc (sizeof (Pilha));
    
    if (plh == NULL)
        return NULL;
    
    plh->vec = vec;
    plh->tam = tam;
    
    plh->prim = -1;
    
    return plh;
}

void vplh_empilhar (Pilha *plh, int info)
{
    
    if (plh == NULL)
        return;
    
    int pos = vplh_obter_memoria_livre (plh);
    
    if (pos < 0)
        return;

//    Elemento *n = (Elemento *) malloc (sizeof (Elemento));
//
//    if (n == NULL)
//        return;
//
//    n->info = info;
//    n->pos = pos;
//    n->uso = 1;
//
//    n->prox = plh->vec[plh->prim].pos;
//
//    plh->vec[pos] = *n;
    
    plh->vec[pos].info = info;
    plh->vec[pos].pos = pos;
    plh->vec[pos].uso = 1;
    
    plh->vec[pos].prox = plh->vec[plh->prim].pos;
    
    plh->prim = pos;
}

int vplh_desempilhar (Pilha *plh, int *info)
{
    
    if (plh == NULL)
        return 0;
    
    if (vplh_esta_vazia (plh))
        return 0;
    
    Elemento t = plh->vec[plh->prim];
    *info = t.info;
    
    plh->prim = t.prox;
    
    plh->vec[t.pos].info = 0;
    plh->vec[t.pos].pos = -1;
    plh->vec[t.pos].uso = 0;
    plh->vec[t.pos].prox = -1;
    
    return 1;
}

void vplh_destruir (Pilha *plh)
{
    
    if (plh == NULL)
        return;
    
    Elemento t, q = plh->vec[plh->prim];
    while (q.uso != 0)
    {
        t = plh->vec[q.prox];
        plh->vec[t.pos].uso = 0;
        //free (q);
        q = t;
    }
    
    free (plh);
}

int vplh_esta_vazia (Pilha *plh)
{
    
    if (plh == NULL || plh->prim < 0)
        return 1;
    
    return 0;
}

#endif //_PILHAVEC_H_
