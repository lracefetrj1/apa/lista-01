#ifndef _L3VETDIN_H_
#define _L3VETDIN_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <search.h>
#include <string.h>
#include <ctype.h>

long int _vtd_size = 0;
long int _vtd_cont = 0;

typedef struct data Data;

struct data {
    
    char *info;
    int qtd;
    
};

Data *vtd_create (long int tam)
{
    _vtd_size = tam;
    return (Data *) malloc (_vtd_size * sizeof (Data));
}

long int vtd_cont (void)
{
    return _vtd_cont;
}

int _vtd_compare_data (char **d1, char **d2)
{
    if (*d2 == NULL)
        return 0;
    
    return strcmp (*d1, *d2);
}

int vtd_binary_search (Data *vtd, char *info)
{
    int inf = 0;
    int sup = _vtd_size - 1;
    int meio, cmp;
    while (inf <= sup)
    {
        _vtd_cont++;
        meio = (inf + sup) / 2;
        cmp = _vtd_compare_data (&info, &vtd[meio].info);
        if (cmp == 0)
            return meio;
        if (cmp < 0)
            sup = meio - 1;
        else
            inf = meio + 1;
    }
    
    return inf;
}

Data *vtd_add_ord (Data *vtd, char *info)
{
    
    int cmp = vtd_binary_search (vtd, info);
    
    if (vtd[cmp].info == NULL)
    {
        vtd[cmp].info = (char *) malloc (sizeof (char) * strlen (info));
        strcpy (vtd[cmp].info, info);
    }
    vtd[cmp].qtd++;
    
    return vtd;
}

void vtd_print (Data *vtd)
{
    if (vtd == NULL)
        return;
    
    for (int i = 0; i < _vtd_size; i++)
    {
        if (vtd[i].info != NULL)
            printf ("\n %s: %d ", vtd[i].info, vtd[i].qtd);
    }
    
    printf ("\n");
}

void vtd_destroy (Data *vtd)
{
    if (vtd == NULL)
        return;
    
    for (int i = 0; i < _vtd_size; i++)
    {
        if (vtd[i].info != NULL)
            free (vtd[i].info);
    }
    
    free (vtd);
    
}

#endif //_L3VETDIN_H_
