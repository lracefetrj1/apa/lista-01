//
// Created by lra on 04/11/22.
//

#ifndef _L3GRAFOMA_H_
#define _L3GRAFOMA_H_

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <math.h>

typedef struct vertex {
    
    int id;
    unsigned int tile;
    
    unsigned int num_neighbors;
    int *neighbors;
} VTX;

typedef struct arcs {
    
    unsigned int link;
    unsigned int cost;
} ARC;

typedef struct graph {
    
    unsigned int num_vertex;
    unsigned int num_arc;
    
    unsigned int num_neighbors;
    
    VTX *vertex;
    ARC **arcs;
} GMA;

// O(n²)
GMA *gma_create (const unsigned int vertices)
{
    
    if (vertices == 0)
        return NULL;
    
    GMA *gma = (GMA *) malloc (sizeof (GMA));
    
    gma->num_vertex = vertices;
    gma->num_arc = 0;
    
    gma->vertex = (VTX *) malloc (gma->num_vertex * sizeof (VTX));
    gma->arcs = (ARC **) malloc (gma->num_vertex * sizeof (ARC *));
    for (int i = 0; i < gma->num_vertex; i++)
    {
        
        gma->vertex[i].id = i;
        gma->vertex[i].tile = 0;
        gma->vertex[i].num_neighbors = 0;
        
        gma->arcs[i] = (ARC *) malloc (gma->num_vertex * sizeof (ARC));
        
        /*
        for (int j = 0; j < gma->num_vertex; j++)
        {
            gma->arcs[i][j].link = 0;
            gma->arcs[i][j].cost = 1;
        }
        */
        
    }
    
    return gma;
}

// O(1)
GMA *gma_add_arc (GMA *gma, const int v1, const int v2, const int cost)
{
    
    if (gma == NULL || (v1 < 0 || v1 > gma->num_vertex) || (v2 < 0 || v2 > gma->num_vertex))
        return gma;
    
    if (gma->arcs[v1][v2].link == 0)
    {
        
        if (gma->vertex[v1].neighbors != NULL)
            gma->vertex[v1].neighbors[gma->vertex[v1].num_neighbors++] = v2;
        
        gma->arcs[v1][v2].link = 1;
        gma->arcs[v1][v2].cost = cost;
        gma->num_arc++;
    }
    return gma;
}

GMA *gma_remove_arc (GMA *gma, const unsigned int v1, const unsigned int v2)
{
    
    if (gma == NULL || v1 > gma->num_vertex || v2 > gma->num_vertex)
        return gma;
    
    if (gma->arcs[v1][v2].link == 1)
    {
        gma->arcs[v1][v2].link = 0;
        gma->arcs[v1][v2].cost = 0;
        gma->num_arc--;
    }
    
    return gma;
}

// O(1)
unsigned int gma_are_neighbors (GMA *gma, const unsigned int v1, const unsigned int v2)
{
    
    if (gma == NULL || v1 > gma->num_vertex || v2 > gma->num_vertex)
        return 0;
    
    return (gma->arcs[v1][v2].link);
}

// O(1)
int *gma_neighbors (const GMA *gma, const unsigned int v)
{
    
    if (gma == NULL || v > gma->num_vertex)
        return NULL;
    
    return gma->vertex[v].neighbors;
}

// O(1)
GMA *gma_add_piece (GMA *gma, const unsigned int v, const unsigned int tile)
{
    
    if (gma == NULL || v > gma->num_vertex)
        return gma;
    
    if (gma->vertex[v].tile == 0)
        gma->vertex[v].tile = tile;
    
    return gma;
}

// O(n²)
void gma_print_arcs (GMA *gma)
{
    if (gma == NULL)
        return;
    
    for (unsigned int i = 0; i < gma->num_vertex; i++)
    {
        for (unsigned int j = 0; j < gma->num_vertex; j++)
        {
            printf ("\t%d", gma->arcs[i][j].link);
        }
        
        printf ("\n");
    }
    
}

static inline void red ()
{
    printf ("\033[1;31m");
}

static inline void yellow ()
{
    printf ("\033[1;33m");
}

static inline void blue ()
{
    printf ("\033[1;34m");
}

static inline void reset ()
{
    printf ("\033[0m");
}

static inline void indent (int n)
{
    for (int i = 0; i < n; i++)
        putchar ('\t');
}

// O(n)
void gma_print (GMA *gma)
{
    if (gma == NULL)
        return;
    
    int rqd = sqrt (gma->num_vertex), level = -1;
    
    for (unsigned int i = 0; i < gma->num_vertex; i++)
    {
        
        if (i % rqd == 0)
        {
            printf ("\n");
            indent (++level);
            
        }
        
        if (gma->vertex[i].tile == 1)
            red ();
        else if (gma->vertex[i].tile == 2)
            blue ();
        else
            reset ();
        
        printf ("(%2d, %2d) ", gma->vertex[i].id, gma->vertex[i].tile);
    }
    reset ();
    
    printf ("\n");
}

static void gma_bepr (const GMA *gma, const unsigned int v, int *ctrl)
{
    ctrl[v] = 1;
    
    printf ("\n%d", v);
    
    for (int i = 1; i <= gma->num_vertex; i++)
        if (gma->arcs[v][i].link == 1 && ctrl[i] == -1)
            gma_bepr (gma, i, ctrl);
    
}

/* busca em profundidade */
void gma_bep (const GMA *gma)
{
    if (gma == NULL)
        return;
    
    int *ctrl = (int *) malloc (gma->num_vertex * sizeof (int));
    if (ctrl == NULL)
        return;
    
    for (int i = 0; i < gma->num_vertex; i++)
        ctrl[i] = -1;
    
    for (int v = 0; v < gma->num_vertex; v++)
        if (ctrl[v] == -1)
            gma_bepr (gma, v, ctrl);
    
    free (ctrl);
}

/* dijkstras short path finder */
void gma_cptd (GMA *gma, const unsigned int v, int *path, unsigned int *dist)
{
    
    if (gma == NULL)
        return;
    
    int *visited = (int *) malloc (gma->num_vertex * sizeof (int));
    
    for (int i = 0; i < gma->num_vertex; i++)
    {
        path[i] = -1;
        visited[i] = 0;
        dist[i] = INT_MAX;
    }
    
    path[v] = v;
    dist[v] = 1;
    
    while (1)
    {
        // escolha de y:
        unsigned int min = INT_MAX;
        int idx = -1;
        
        for (int i = 0; i < gma->num_vertex; i++)
        {
            if (visited[i] == 0 && dist[i] < min)
            {
                min = dist[i];
                idx = i;
            }
        }
    
        if (idx == -1 || min == INT_MAX)
            break;
        
        // atualização de dist[] e pa[]:
        ARC *nb = gma->arcs[idx];
        
        for (int i = 0; i < gma->num_vertex; i++)
        {
            if (nb[i].link == 1 && gma->vertex[i].tile == 0 && visited[i] == 0)
            {
    
                if ((dist[idx] + nb[i].cost) < dist[i])
                {
                    dist[i] = dist[idx] + nb[i].cost;
                    path[i] = idx;
                }
            }
    
        }
    
        visited[idx] = 1;
    }
    
    free (visited);
}

static inline int gma_min (int dx, int dy)
{
    return dx > dy ? dy : dx;
}

static double gma_astarh_manhattan (GMA *gma, const unsigned int v1, const unsigned int v2)
{
    int rq = sqrt (gma->num_vertex);
    
    unsigned int v1x = (v1 / rq);
    unsigned int v1y = (v1 % rq);
    
    unsigned int v2x = (v2 / rq);
    unsigned int v2y = (v2 % rq);
    
    int dx = (v1x - v2x);
    int dy = (v1y - v2y);
    
    return (abs (dx + dy));
}

static double gma_astarh_diagonal (GMA *gma, const unsigned int v1, const unsigned int v2)
{
    int rq = sqrt (gma->num_vertex);
    
    int v1x = (v1 / rq);
    int v1y = (v1 % rq);
    
    int v2x = (v2 / rq);
    int v2y = (v2 % rq);
    
    int dx = abs (v1x - v2x);
    int dy = abs (v1y - v2y);
    
    int D1 = 1;
    double D2 = sqrt (2);
    
    return (D1 * (dx + dy) + (D2 - 2 * D1) * gma_min (dx, dy));
}

static double gma_astarh_euclidean (GMA *gma, const unsigned int v1, const unsigned int v2)
{
    int rq = sqrt (gma->num_vertex);
    
    unsigned int v1x = (v1 / rq);
    unsigned int v1y = (v1 % rq);
    
    unsigned int v2x = (v2 / rq);
    unsigned int v2y = (v2 % rq);
    
    double dx = pow (v1x - v2x, 2);
    double dy = pow (v1y - v2y, 2);
    
    return sqrt (dx + dy);
}

void
gma_astar (GMA *gma, double (*heuristic) (GMA *, const unsigned int, const unsigned int), const unsigned int v1, const unsigned int v2, int *path, unsigned int *dist)
{
    
    if (gma == NULL)
        return;
    
    int *visited = (int *) malloc (gma->num_vertex * sizeof (int));
    
    double *prrt = (double *) malloc (gma->num_vertex * sizeof (double));
    
    for (int i = 0; i < gma->num_vertex; i++)
    {
        visited[i] = 0;
        dist[i] = INT_MAX;
        prrt[i] = INT_MAX;
        path[i] = -1;
    }
    
    path[v1] = v1;
    dist[v1] = 1;
    
    prrt[v1] = (*heuristic) (gma, v1, v2);
    
    while (1)
    {
        
        double min = INT_MAX;
        int idx = -1;
        for (int i = 0; i < gma->num_vertex; i++)
        {
            if (prrt[i] < min && !visited[i])
            {
                min = prrt[i];
                idx = i;
            }
        }
    
        if (idx == -1 || idx == v2 || min == INT_MAX)
            break;
        
        for (int i = 0; i < gma->num_vertex; i++)
        {
            if (gma->arcs[idx][i].link == 1 && gma->vertex[idx].tile == 0 && visited[i] == 0)
            {
                if (dist[idx] + gma->arcs[idx][i].cost < dist[i])
                {
                    dist[i] = dist[idx] + gma->arcs[idx][i].cost;
                    prrt[i] = dist[i] + (*heuristic) (gma, i, v2);
                    path[i] = idx;
                }
            }
        }
        
        visited[idx] = 1;
    }
    
    free (visited);
}

void gma_destroy (GMA *gma)
{
    if (gma == NULL)
        return;
    
    for (unsigned int i = 0; i < gma->num_vertex; i++)
    {
        if (gma->vertex[i].num_neighbors > 0)
            free (gma->vertex[i].neighbors);
        
        free (gma->arcs[i]);
    }
    
    free (gma->vertex);
    free (gma->arcs);
    free (gma);
    
}

#endif //_L3GRAFOMA_H_
