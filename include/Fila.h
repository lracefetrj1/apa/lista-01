//
// Created by lra on 19/10/22.
//

#ifndef _FILA_H_
#define _FILA_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

typedef struct fila Fila;
typedef struct lista Lista;
typedef struct elemento Elemento;

struct elemento {
    int id;
    char *nome;
    char *orig;
    char *dest;
};

struct lista {
    Elemento *info;
    
    Lista *prox;
};

struct fila {
    Lista *ini;
    Lista *fim;
};

Elemento *fla_criar_elemento (int id, char *nome, char *orig, char *dest)
{
    Elemento *e = (Elemento *) malloc (sizeof (Elemento));
    
    e->id = id;
    e->nome = nome;
    e->orig = orig;
    e->dest = dest;
    
    return e;
}

Fila *fla_criar (void)
{
    Fila *f = (Fila *) malloc (sizeof (Fila));
    f->ini = f->fim = NULL;
    return f;
}

int fla_esta_vazia (Fila *f)
{
    return (f == NULL || f->ini == NULL || f->fim == NULL);
}

void fla_inserir (Fila *f, Elemento *e)
{
    Lista *n = (Lista *) malloc (sizeof (Lista));
    
    n->info = e;
    n->prox = NULL;
    
    if (f->fim != NULL)
        f->fim->prox = n;
    else
        f->ini = n;
    
    f->fim = n;
}

int fla_remover (Fila *f, Elemento *info)
{
    Lista *t;
    
    if (fla_esta_vazia (f))
        return 0;
    
    t = f->ini;
    *info = *t->info;
    
    f->ini = t->prox;
    if (f->ini == NULL)
        f->fim = NULL;
    
    free (t);
    
    return 1;
}

void fla_destruir (Fila *f)
{
    Lista *q = f->ini;
    while (q != NULL)
    {
        Lista *t = q->prox;
        free (q);
        q = t;
    }
    free (f);
}

void fla_imprimir_elemento (Elemento *e)
{
    printf ("id: %d, nome: %s, origem: %s, destino: %s \n", e->id, e->nome, e->orig, e->dest);
}

void fla_imprimir (Lista *l)
{
    if (l != NULL)
    {
        fla_imprimir_elemento (l->info);
        fla_imprimir (l->prox);
    }
}

int fla_quantidade_elementos (Fila *f)
{
    Lista *l = f->ini;
    
    int c = 0;
    while (l != NULL)
    {
        l = l->prox;
        c++;
    }
    
    return c;
}

Elemento *fla_autorizar_decolagem (Fila *f)
{
    Elemento *e = (Elemento *) malloc (sizeof (Elemento));;
    fla_remover (f, e);
    return e;
}

void fla_caracteristica_aviao (Fila *f)
{
    fla_imprimir_elemento (f->ini->info);
}

void fla_fila_espera (Fila *f)
{
    fla_imprimir (f->ini);
}

#endif //_FILA_H_
