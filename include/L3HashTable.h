#ifndef _L3HASHTABLE_H_
#define _L3HASHTABLE_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <search.h>
#include <string.h>
#include <ctype.h>

size_t __hsh_size = 0;

typedef struct hash HashTable;

struct hash {
    char *key;
    void *value;
};

HashTable *hsh_create (size_t tam)
{
    __hsh_size = tam;
    return (HashTable *) malloc (__hsh_size * sizeof (HashTable));
}

/* Variante de D. J. Bernstein hash */
static size_t __hsh_hash (const char *key)
{
    size_t hash = 5381;
    size_t pos = 2;
    while (*key)
        hash = 33 * hash ^ (unsigned char) *key++ + pos++;
    return hash % __hsh_size;
}

/* Não trata colisão. Sempre atualiza */
HashTable *hsh_add (HashTable *hsh, char *key, void *value)
{
    
    size_t cmp = __hsh_hash (key);
    
    hsh[cmp].key = key;
    hsh[cmp].value = value;
    
    return hsh;
}

const void *hsh_get (HashTable *hsh, const char *key)
{
    if (hsh == NULL || key == NULL)
        return NULL;
    
    return hsh[__hsh_hash (key)].value;
}

void hsh_print (HashTable *hsh)
{
    if (hsh == NULL)
        return;
    
    for (int i = 0; i < __hsh_size; i++)
    {
        if (hsh[i].key != NULL)
            printf ("(%s, %p)\n", hsh[i].key, hsh[i].value);
    }
    
    printf ("\n");
}

void hsh_destroy (HashTable *hsh)
{
    if (hsh == NULL)
        return;
    
    free (hsh);
    
}

#endif //_L3HASHTABLE_H_
