#ifndef _L3PILHA_H_
#define _L3PILHA_H_

#include <stdlib.h>
#include <string.h>

typedef struct pilha Pilha;

int plh_esta_vazia (Pilha *p);

struct elemento {
    char *info;
    struct elemento *prox;
};

typedef struct elemento Elemento;

struct pilha {
    Elemento *prim;
};

Pilha *plh_criar (void)
{
    Pilha *p = (Pilha *) malloc (sizeof (Pilha));
    
    if (p == NULL)
        return NULL;
    
    p->prim = NULL;
    return p;
}

void plh_empilhar (Pilha *p, char *info)
{
    
    if (p == NULL)
        return;
    
    Elemento *n = (Elemento *) malloc (sizeof (Elemento));
    
    if (n == NULL)
        return;
    
    n->info = (char *) malloc (strlen (info) * sizeof (char));
    strcpy (n->info, info);
    
    n->prox = p->prim;
    p->prim = n;
}

int plh_desempilhar (Pilha *p, char **info)
{
    
    if (p == NULL)
        return 0;
    
    if (plh_esta_vazia (p))
        return 0;
    
    Elemento *t = p->prim;
    *info = t->info;
    
    p->prim = t->prox;
    free (t->info);
    free (t);
    
    return 1;
}

void plh_destruir (Pilha *p)
{
    
    if (p == NULL)
        return;
    
    Elemento *t, *q = p->prim;
    while (q != NULL)
    {
        t = q->prox;
        free (q->info);
        free (q);
        q = t;
    }
    free (p);
}

int plh_esta_vazia (Pilha *p)
{
    
    if (p == NULL || p->prim == NULL)
        return 1;
    
    return 0;
}

#endif //_L3PILHA_H_
