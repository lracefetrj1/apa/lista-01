#ifndef _L3LISTA_H_
#define _L3LISTA_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <search.h>
#include <string.h>
#include <ctype.h>

long int _lst_size = 0;
long int _lst_cont = 0;

typedef struct list List;

struct list {
    
    char *data;
    int qtd;
    
    List *next;
};

long int lst_size (void)
{
    return _lst_size;
}

long int lst_cont (void)
{
    return _lst_cont;
}

List *lst_create (void)
{
    _lst_size = 0;
    _lst_cont = 0;
    
    return NULL;
}

int _lst_compare_data (char **d1, char **d2)
{
    if (*d2 == NULL)
        return 0;
    
    return strcmp (*d1, *d2);
}

List *lst_add (List *lst, char *data)
{
    List *tmp = (List *) malloc (sizeof (List));
    
    if (tmp == NULL)
        return lst;
    
    tmp->data = (char *) malloc (sizeof (char) * strlen (data));
    strcpy (tmp->data, data);
    tmp->next = lst;
    
    lst = tmp;
    
    _lst_size++;
    
    return lst;
}

List *lst_add_ord (List *lst, char *data)
{
    List *ant = NULL;
    List *tmp = lst;
    
    int cmp;
    while (tmp != NULL)
    {
        _lst_cont++;
        cmp = _lst_compare_data (&tmp->data, &data);
        if (cmp > 0)
        {
            ant = tmp;
            tmp = tmp->next;
        }
        else
            break;
    }
    
    if (tmp != NULL && cmp == 0)
    {
        tmp->qtd++;
        return lst;
    }
    
    List *p = (List *) malloc (sizeof (List));
    
    p->qtd = 1;
    p->data = (char *) malloc (sizeof (char) * strlen (data));
    strcpy (p->data, data);
    p->next = tmp;
    
    if (ant == NULL)
    {
        lst = p;
    }
    else
    {
        ant->next = p;
    }
    
    _lst_size++;
    
    return lst;
}

void lst_print (List *lst)
{
    if (lst == NULL)
        return;
    
    List *tmp = lst;
    while (tmp != NULL)
    {
        printf ("\n %s: %d ", tmp->data, tmp->qtd);
        tmp = tmp->next;
    }
    printf ("\n");
}

void lst_destroy (List *lst)
{
    if (lst == NULL)
        return;
    
    List *tmp = NULL;
    
    while (lst != NULL)
    {
        tmp = lst->next;
        free (lst->data);
        free (lst);
        lst = tmp;
    }
    
}

#endif //_L3LISTA_H_
