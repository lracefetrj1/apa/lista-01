//
// Created by lra on 16/10/22.
//

#ifndef _PILHA_H_
#define _PILHA_H_

#include <stdlib.h>

/* TAD: pilha de valores reais (float) */
typedef struct pilha Pilha;

Pilha *plh_criar (void);
void plh_empilhar (Pilha *p, int info);
int plh_desempilhar (Pilha *p, int *info);
int plh_esta_vazia (Pilha *p);
void plh_destruir (Pilha *p);

/* nó da lista para armazenar valores reais */
struct elemento {
    int info;
    struct elemento *prox;
};

typedef struct elemento Elemento;

/* estrutura da pilha */
struct pilha {
    Elemento *prim; /* aponta para o topo da pilha */
};

Pilha *plh_criar (void)
{
    Pilha *p = (Pilha *) malloc (sizeof (Pilha));
    
    if (p == NULL)
        return NULL;
    
    p->prim = NULL;
    return p;
}

void plh_empilhar (Pilha *p, int info)
{
    
    if (p == NULL)
        return;
    
    Elemento *n = (Elemento *) malloc (sizeof (Elemento));
    
    if (n == NULL)
        return;
    
    n->info = info;
    n->prox = p->prim;
    p->prim = n;
}

int plh_desempilhar (Pilha *p, int *info)
{
    
    if (p == NULL)
        return 0;
    
    if (plh_esta_vazia (p))
        return 0;
    
    Elemento *t = p->prim;
    *info = t->info;
    
    p->prim = t->prox;
    free (t);
    
    return 1;
}

void plh_destruir (Pilha *p)
{
    
    if (p == NULL)
        return;
    
    Elemento *t, *q = p->prim;
    while (q != NULL)
    {
        t = q->prox;
        free (q);
        q = t;
    }
    free (p);
}

int plh_esta_vazia (Pilha *p)
{
    
    if (p == NULL || p->prim == NULL)
        return 1;
    
    return 0;
}

#endif //_PILHA_H_
