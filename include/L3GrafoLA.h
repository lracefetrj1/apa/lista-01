#ifndef _L3GRAFOLA_H_
#define _L3GRAFOLA_H_

#include <stdlib.h>
#include <stdio.h>

typedef struct graph GLA;
typedef struct list LTA;

struct graph {
    
    unsigned int num_vertices;
    unsigned int num_arcs;
    
    LTA **vector;
};

struct list {
    
    unsigned int vertice;
    
    LTA *next;
};

GLA *gla_create (const unsigned int vertices)
{
    
    if (vertices == 0)
        return NULL;
    
    GLA *gma = (GLA *) malloc (sizeof (GLA));
    
    gma->num_vertices = vertices;
    gma->num_arcs = 0;
    
    gma->vector = (LTA **) malloc (gma->num_vertices * sizeof (LTA *));
    for (int i = 0; i < gma->num_vertices; i++)
        gma->vector[i] = NULL;
    
    return gma;
}

GLA *gla_add_arc (GLA *gla, unsigned int v1, unsigned int v2)
{
    if (gla == NULL || v1 > gla->num_vertices || v2 > gla->num_vertices)
        return gla;
    
    for (LTA *lta = gla->vector[(v1 - 1)]; lta != NULL; lta = lta->next)
        if (lta->vertice == v2)
            return gla;
    
    LTA *lta = (LTA *) malloc (sizeof (LTA));
    lta->vertice = v2;
    lta->next = gla->vector[(v1 - 1)];
    gla->vector[(v1 - 1)] = lta;
    
    gla->num_arcs++;
    
    return gla;
}

GLA *gla_remove_arc (GLA *gla, const unsigned int v1, const unsigned int v2)
{
    
    if (gla == NULL || v1 > gla->num_vertices || v2 > gla->num_vertices)
        return gla;
    
    LTA *ant = NULL;
    LTA *lta = gla->vector[(v1 - 1)];
    
    while (lta != NULL && lta->vertice != v2)
    {
        ant = lta;
        lta = lta->next;
    }
    
    if (lta == NULL)
        return gla;
    
    if (ant == NULL)
        gla->vector[(v1 - 1)] = lta->next;
    else
        ant->next = lta->next;
    
    free (lta);
    
    gla->num_arcs--;
    
    return gla;
}

unsigned int gla_are_neighbors (GLA *gla, const unsigned int v1, const unsigned int v2)
{
    
    if (gla == NULL || v1 > gla->num_vertices || v2 > gla->num_vertices)
        return 0;
    
    LTA *lta = gla->vector[(v1 - 1)];
    while (lta != NULL && lta->vertice != v2)
        lta = lta->next;
    
    return (lta ? 1 : 0);
    // return (gma->matriz[(v1 - 1)][(v2 - 1)] || gma->matriz[(v2 - 1)][(v1 - 1)]);
}

LTA *gla_neighbors (const GLA *gla, const unsigned int v)
{
    
    if (gla == NULL || v > gla->num_vertices)
        return NULL;
    
    return gla->vector[(v - 1)];
}

static void gla_bepr (const GLA *gla, const unsigned int v, unsigned int *ctrl)
{
    ctrl[(v - 1)] = 1;
    
    printf ("\n%d", v);
    
    LTA *lta = gla->vector[(v - 1)];
    while (lta)
    {
        if (ctrl[(lta->vertice - 1)] == -1)
            gla_bepr (gla, lta->vertice, ctrl);
        lta = lta->next;
    }
    
}

/* busca em profundidade */
void gla_bep (const GLA *gla)
{
    unsigned int *ctrl = (unsigned int *) malloc (gla->num_vertices * sizeof (unsigned int));
    if (ctrl == NULL)
        return;
    
    for (int i = 0; i < gla->num_vertices; i++)
        ctrl[i] = -1;
    
    for (int v = 1; v <= gla->num_vertices; ++v)
        if (ctrl[(v - 1)] == -1)
            gla_bepr (gla, v, ctrl);
    
    free (ctrl);
}

void gla_print (GLA *gla)
{
    if (gla == NULL)
        return;
    
    for (int i = 0; i < gla->num_vertices; i++)
    {
        
        printf ("\t%d - ", (i + 1));
        
        for (LTA *lta = gla->vector[i]; lta != NULL; lta = lta->next)
            printf ("%d\t", lta->vertice);
        
        printf ("\n");
    }
    
}

void gla_destroy (GLA *gla)
{
    if (gla == NULL)
        return;
    
    LTA *lta = NULL, *tmp = NULL;
    for (int i = 0; i < gla->num_vertices; i++)
    {
        lta = gla->vector[i];
        while (lta != NULL)
        {
            tmp = lta->next;
            free (lta);
            lta = tmp;
        }
    }
    
    free (gla->vector);
    free (gla);
    
}

#endif //_L3GRAFOLA_H_
