//
// Created by lra on 02/11/22.
//

#ifndef _L3ARVOREBIN_H_
#define _L3ARVOREBIN_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct arv Arv;
typedef struct arv_data ArvData;

struct arv_data {
    
    char *info;
    int qtd;
    
};

struct arv {
    ArvData *data;
    
    Arv *esq;
    Arv *dir;
};

long int _arv_cont = 0;
long int _arv_cont_avl = 0;

int arv_is_empty (Arv *arv);
int arv_dif_altura (Arv *arv);

Arv *arv_rot_left (Arv *arv);
Arv *arv_rot_right (Arv *arv);

Arv *arv_create_empty (void)
{
    
    _arv_cont = 0;
    _arv_cont_avl = 0;
    return NULL;
}

int _arv_compare_data (char **d1, char **d2)
{
    if (*d2 == NULL)
        return 0;
    
    return strcmp (*d1, *d2);
}

long int arv_cont (void)
{
    return _arv_cont;
}

long int arv_cont_avl (void)
{
    return _arv_cont_avl;
}

Arv *arv_add (Arv *arv, char *info)
{
    _arv_cont++;
    if (arv == NULL)
    {
        arv = (Arv *) malloc (sizeof (Arv));
        
        ArvData *data = (ArvData *) malloc (sizeof (ArvData));
        
        data->qtd = 1;
        data->info = (char *) malloc (sizeof (char) * strlen (info));
        strcpy (data->info, info);
        
        arv->data = data;
        arv->esq = arv->dir = NULL;
    }
    else
    {
        int cmp = _arv_compare_data (&info, &arv->data->info);
        
        if (cmp == 0)
        {
            arv->data->qtd++;
        }
        else if (cmp < 0)
            arv->esq = arv_add (arv->esq, info);
        else
            arv->dir = arv_add (arv->dir, info);
    }
    
    return arv;
}

Arv *arv_rot_left (Arv *arv)
{
    int harv = arv_dif_altura (arv->esq);
    if (harv < -1)
        arv = arv_rot_right (arv);
    
    Arv *tmp = arv->dir;
    arv->dir = tmp->esq;
    tmp->esq = arv;
    
    return tmp;
}

Arv *arv_rot_right (Arv *arv)
{
    int harv = arv_dif_altura (arv->dir);
    if (harv > 1)
        arv = arv_rot_left (arv);
    
    Arv *tmp = arv->esq;
    arv->esq = tmp->dir;
    tmp->dir = arv;
    
    return tmp;
}

Arv *arv_add_avl (Arv *arv, char *info)
{
    
    _arv_cont_avl++;
    if (arv == NULL)
    {
        arv = (Arv *) malloc (sizeof (Arv));
        
        ArvData *data = (ArvData *) malloc (sizeof (ArvData));
        
        data->qtd = 1;
        data->info = (char *) malloc (sizeof (char) * strlen (info));
        strcpy (data->info, info);
        
        arv->data = data;
        arv->esq = arv->dir = NULL;
        
        int harv = arv_dif_altura (arv);
        if (harv < -1)
            arv = arv_rot_left (arv);
        else if (harv > 1)
            arv = arv_rot_right (arv);
        
    }
    else
    {
        int cmp = _arv_compare_data (&info, &arv->data->info);
        
        if (cmp == 0)
        {
            arv->data->qtd++;
        }
        else if (cmp < 0)
            arv->esq = arv_add (arv->esq, info);
        else
            arv->dir = arv_add (arv->dir, info);
    }
    
    return arv;
}

void arv_destroy (Arv *arv)
{
    if (!arv_is_empty (arv))
    {
        arv_destroy (arv->esq);
        arv_destroy (arv->dir);
        free (arv->data);
        free (arv);
    }
}

int arv_is_empty (Arv *arv)
{
    return (arv == NULL);
}

int __maior (int a, int b)
{
    if (a > b)
        return a;
    else
        return b;
}

int arv_altura (Arv *arv)
{
    if ((arv == NULL) || (arv->esq == NULL && arv->dir == NULL))
        return 0;
    else
        return 1 + __maior (arv_altura (arv->esq), arv_altura (arv->dir));
}

int arv_dif_altura (Arv *arv)
{
    if ((arv == NULL) || (arv->esq == NULL && arv->dir == NULL))
        return 0;
    else
        return (arv_altura (arv->esq) - arv_altura (arv->dir));
}

void arv_print (Arv *arv)
{
    if (!arv_is_empty (arv))
    {
        printf ("\n %s: %d ", arv->data->info, arv->data->qtd);
        arv_print (arv->esq);
        arv_print (arv->dir);
    }
    
}

#endif //_L3ARVOREBIN_H_
