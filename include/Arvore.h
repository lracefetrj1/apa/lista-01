//
// Created by lra on 18/10/22.
//

#ifndef _ARVORE_H_
#define _ARVORE_H_

#include <stdlib.h>
#include <stdio.h>

typedef struct arv Arv;

struct arv {
    char info;
    int nivel;
    
    Arv *esq;
    Arv *dir;
};

int arv_esta_vazia (Arv *);

Arv *arv_criar_vazia (void)
{
    return NULL;
}

Arv *arv_criar (char c, Arv *sae, Arv *sad)
{
    Arv *p = (Arv *) malloc (sizeof (Arv));
    
    p->info = c;
    
    p->esq = sae;
    p->dir = sad;
    
    return p;
}

Arv *arv_destruir (Arv *a)
{
    if (!arv_esta_vazia (a))
    {
        arv_destruir (a->esq);
        arv_destruir (a->dir);
        free (a);
    }
    return NULL;
}

int arv_esta_vazia (Arv *a)
{
    return (a == NULL);
}

int __maior (int a, int b)
{
    if (a > b)
        return a;
    else
        return b;
}

int arv_altura (Arv *a)
{
    if ((a == NULL) || (a->esq == NULL && a->dir == NULL))
        return 0;
    else
        return 1 + __maior (arv_altura (a->esq), arv_altura (a->dir));
}

int arv_dif_altura (Arv *a)
{
    if ((a == NULL) || (a->esq == NULL && a->dir == NULL))
        return 0;
    else
        return (arv_altura (a->esq) - arv_altura (a->dir));
}

int arv_buscar (Arv *a, char c)
{
    if (arv_esta_vazia (a))
        return 0;
    else
        return (a->info == c ||
                arv_buscar (a->esq, c) ||
                arv_buscar (a->dir, c));
}

void arv_imprimir (Arv *a)
{
    if (!arv_esta_vazia (a))
    {
        printf ("%c ", a->info);
        arv_imprimir (a->esq);
        arv_imprimir (a->dir);
    }
}

void arv_imprimir_nt (Arv *a)
{
    printf ("<");
    if (!arv_esta_vazia (a))
    {
        printf ("%c ", a->info);
        arv_imprimir_nt (a->esq);
        arv_imprimir_nt (a->dir);
    }
    printf (">");
}

void arv_imprimir_sim (Arv *a)
{
    if (a != NULL)
    {
        arv_imprimir_sim (a->esq);
        /* mostra sae */
        printf ("%c ", a->info);
        /* mostra raiz */
        arv_imprimir_sim (a->dir);
        /* mostra sad */
    }
}

void arv_imprimir_pos (Arv *a)
{
    if (a != NULL)
    {
        arv_imprimir_pos (a->esq);
        /* mostra sae */
        arv_imprimir_pos (a->dir);
        /* mostra sad */
        printf ("%c ", a->info);
        /* mostra raiz */ }
}

void arv_imprimir_pre (Arv *a)
{
    if (a != NULL)
    {
        printf ("%c ", a->info);
        /* mostra raiz */
        
        arv_imprimir_pre (a->esq);
        /* mostra sae */
        
        arv_imprimir_pre (a->dir);
        /* mostra sad */
    }
}

int arv_estritamente_binaria (Arv *a)
{
    if (a != NULL)
    {
        
        if ((a->esq == NULL && a->dir != NULL) || (a->esq != NULL && a->dir == NULL))
            return 0;
        
        int r = 1;
        r = arv_estritamente_binaria (a->esq);
        if (r == 0)
            return r;
        
        r = arv_estritamente_binaria (a->dir);
        if (r == 0)
            return r;
    }
    
    return 1;
}

int arv_quase_completa (Arv *a)
{
    if (a != NULL)
    {
        if (arv_estritamente_binaria (a) == 0)
            return 0;
        
        if (abs (arv_dif_altura (a)) > 1)
            return 0;
        
        int r = 1;
        r = arv_quase_completa (a->esq);
        if (r == 0)
            return r;
        
        r = arv_quase_completa (a->dir);
        if (r == 0)
            return r;
    }
    
    return 1;
}

int arv_completa (Arv *a)
{
    if (a != NULL)
    {
        if (arv_estritamente_binaria (a) == 0)
            return 0;
        
        if (abs (arv_dif_altura (a)) != 0)
            return 0;
        
        int r = 1;
        r = arv_quase_completa (a->esq);
        if (r == 0)
            return r;
        
        r = arv_quase_completa (a->dir);
        if (r == 0)
            return r;
    }
    
    return 1;
}

int arv_similar (Arv *a, Arv *b)
{
    if (a != NULL && b != NULL)
    {
        if (a->info != b->info)
            return 0;
        
        int r = 1;
        r = arv_similar (a->esq, b->esq);
        if (r == 0)
            return r;
        
        r = arv_similar (a->dir, b->dir);
        if (r == 0)
            return r;
    }
    else if (a == NULL && b == NULL)
        return 1;
    else
        return 0;
    
    return 1;
}

#endif //_ARVORE_H_
