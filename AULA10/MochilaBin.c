//
// Created by lra on 26/11/22.
//
//
// Created by lra on 26/11/22.
//
#include <stdlib.h>
#include <stdio.h>

static int c = 50, i = 4;
static int pi[] = {40, 30, 20, 10, 20};
static int vi[] = {840, 600, 400, 100, 300};

//static int c = 7, i = 3;
//static int pi[] = {2, 1, 6, 5};
//static int vi[] = {10, 7, 25, 24};

inline static int max (int m, int n)
{
    return (n >= m) ? n : m;
}

static long f (int i, int p)
{
    
    if (i < 0)
        return 0;
    
    long m = 0;
    if (pi[i] > p)
        m += f (i - 1, p);
    else
        m += max (f (i - 1, p - pi[i]) + vi[i], f (i - 1, p));
    
    return m;
}

int main (void)
{
    long m = f (i, c);
    printf ("\nMax = %ld\n", m);
    
    return EXIT_SUCCESS;
}

