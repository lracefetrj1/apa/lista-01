//
// Created by lra on 26/11/22.
//
#include <stdlib.h>
#include <stdio.h>

#define __CACHE__ 1000

static long cache[__CACHE__];

long fib (int n)
{
    if (cache[n])
        return cache[n];
    
    long f;
    if (n <= 2)
        f = 1;
    else
    {
        f = fib (n - 1) + fib (n - 2);
    }
    
    cache[n] = f;
    
    return f;
}

int main (void)
{
    int n = 10;
    long f = fib (n);
    printf ("Fib de %d é %ld\n", n, f);
    
    return EXIT_SUCCESS;
}

