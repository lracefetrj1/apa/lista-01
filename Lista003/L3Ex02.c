#include <stdio.h>
#include <stdlib.h>
#include "../include/L3HashTable.h"

typedef struct elem {
    char *t;
    int p;
} Elem;

int main (void)
{
    
    Elem *e = (Elem *) malloc (sizeof (Elem));
    e->t = "struct elem";
    e->p = 102;
    
    HashTable *hash = hsh_create (7);
    
    hash = hsh_add (hash, "Pato", "com laranja");
    hash = hsh_add (hash, "Elementos", e);
    hash = hsh_add (hash, "Dez", 10);
    hash = hsh_add (hash, "H", 'H');
    
    printf ("Imprimir Hash Completo:\n");
    hsh_print (hash);
    
    printf ("Imprimir Chave -> Valor:\n");
    printf ("Chave: %s -> Valor: %s\n", "Pato", hsh_get (hash, "Pato"));
    
    Elem *f = (Elem *) hsh_get (hash, "Elementos");
    printf ("Chave: %s -> Valor: (%s, %d)\n", "Elementos", f->t, f->p);
    
    printf ("Chave: %s -> Valor: %d\n", "Dez", (int *) hsh_get (hash, "Dez"));
    printf ("Chave: %s -> Valor: %c\n", "H", (char *) hsh_get (hash, "H"));
    
    free (e);
    
    hsh_destroy (hash);
    
    printf ("\nFim!\n");
    
    return EXIT_SUCCESS;
}
