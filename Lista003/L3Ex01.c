#include <stdio.h>
#include <stdlib.h>
#include "../include/L3Lista.h"
#include "../include/L3VetDin.h"
#include "../include/L3ArvoreBin.h"
#include <time.h>

int main (int argc, char **argv)
{
    
    if (argc < 2)
    {
        printf ("Modo de usar: ./L3Ex01 <nome do arquivo> <estrutura>\n");
        printf ("\n");
        printf ("Estruturas: lista_encadeada, pesquisa_binaria, arvore_binaria e arvore_avl \n");
        printf ("\t Exemplo: ./L3Ex01 entrada.txt pesquisa_binaria \n");
        printf ("\n");
        return EXIT_FAILURE;
    }
    
    char *fn = *++argv;
    char *et = *++argv;
    
    if (fn == NULL)
    {
        printf ("Nome do arquivo inválido.\n");
        printf ("\t Exemplo: ./L3Ex01 entrada.txt pesquisa_binaria \n");
        return EXIT_FAILURE;
    }
    
    FILE *fp = NULL;
    
    if ((fp = fopen (fn, "r")) == NULL)
    {
        printf ("Não é possível abrir o arquivo %s.\n", fn);
        return EXIT_FAILURE;
    }
    
    if (et == NULL)
    {
        et = (char *) malloc (16 * sizeof (char));
        et = "lista_encadeada";
    }
    
    int ch, v = 0;
    char data[50];
    
    List *palavras = lst_create ();
    while ((ch = getc (fp)) != EOF)
    {
        if (ispunct(ch) || isblank(ch) || ch == '\n')
        {
            if (v == 0) continue;
            data[v++] = '\0';
            v = 0;
            
            palavras = lst_add (palavras, data);
        }
        else
            data[v++] = tolower (ch);
    }
    
    fclose (fp);
    
    printf ("Relatório: \n\n");
    
    List *tmp = palavras;
    if (strcmp (et, "pesquisa_binaria") == 0)
    {
        clock_t t = clock ();
        
        Data *vetor = vtd_create (lst_size ());
        while (palavras != NULL)
        {
            vetor = vtd_add_ord (vetor, palavras->data);
            palavras = palavras->next;
        }
        t = clock () - t;
        
        printf ("Pesquisa Binária: \n\n");
        printf ("\t%ld comparações\n", vtd_cont ());
        printf ("\t%lf milisegundos\n", ((double) t) / ((CLOCKS_PER_SEC / 1000)));
        
        printf ("\nFrequencia das palavras: \n");
        vtd_print (vetor);
        
        vtd_destroy (vetor);
        
    }
    else if (strcmp (et, "arvore_binaria") == 0)
    {
        clock_t t = clock ();
        
        Arv *arvore = arv_create_empty ();
        
        while (palavras != NULL)
        {
            arvore = arv_add (arvore, palavras->data);
            palavras = palavras->next;
        }
        t = clock () - t;
        
        printf ("Árvore Binária: \n\n");
        printf ("\t%ld comparações\n", arv_cont ());
        printf ("\t%lf milisegundos\n", ((double) t) / ((CLOCKS_PER_SEC / 1000)));
        
        printf ("\nFrequencia das palavras: \n");
        arv_print (arvore);
        printf ("\n");
        
        arv_destroy (arvore);
    }
    else if (strcmp (et, "arvore_avl") == 0)
    {
        clock_t t = clock ();
        
        Arv *arvavl = arv_create_empty ();
        
        while (palavras != NULL)
        {
            arvavl = arv_add_avl (arvavl, palavras->data);
            palavras = palavras->next;
        }
        t = clock () - t;
        
        printf ("Árvore AVL: \n\n");
        printf ("\t%ld comparações\n", arv_cont_avl ());
        printf ("\t%lf milisegundos\n", ((double) t) / ((CLOCKS_PER_SEC / 1000)));
        
        printf ("\nFrequencia das palavras: \n");
        arv_print (arvavl);
        printf ("\n");
        
        arv_destroy (arvavl);
    }
    else
    {
        clock_t t = clock ();
        
        List *lista = lst_create ();
        
        while (palavras != NULL)
        {
            lista = lst_add_ord (lista, palavras->data);
            palavras = palavras->next;
        }
        t = clock () - t;
        
        printf ("Lista Encadeada: \n\n");
        printf ("\t%ld comparações\n", lst_cont ());
        printf ("\t%lf milisegundos\n", ((double) t) / ((CLOCKS_PER_SEC / 1000)));
        
        printf ("\nFrequencia das palavras: \n");
        lst_print (lista);
        
        lst_destroy (lista);
    }
    palavras = tmp;
    
    lst_destroy (palavras);
    
    printf ("\nFim!\n");
    return EXIT_SUCCESS;
}
