//
// Created by lra on 04/11/22.
//

#include <stdio.h>
#include <stdlib.h>
#include "../include/L3GrafoMA.h"

int main (void)
{
    
    GMA *gma = gma_create (7);
    
    gma = gma_add_arc (gma, 2, 1);
    gma = gma_add_arc (gma, 1, 5);
    gma = gma_add_arc (gma, 1, 6);
    gma = gma_add_arc (gma, 5, 3);
    gma = gma_add_arc (gma, 5, 4);
    
    printf ("\nMatriz de Adjacências:\n");
    gma_print (gma);
    
    printf ("\nSão vizinhos (%d, %d): %s\n", 1, 3, gma_are_neighbors (gma, 1, 3) ? "Sim" : "Não");
    printf ("\nSão vizinhos (%d, %d): %s\n", 1, 5, gma_are_neighbors (gma, 1, 5) ? "Sim" : "Não");
    printf ("\nSão vizinhos (%d, %d): %s\n", 2, 1, gma_are_neighbors (gma, 2, 1) ? "Sim" : "Não");
    printf ("\nSão vizinhos (%d, %d): %s\n", 5, 5, gma_are_neighbors (gma, 5, 5) ? "Sim" : "Não");
    
    const unsigned int *neigbors = gma_neighbors (gma, 1);
    printf ("\nVizinhos do vertice %d, antes da remoção\n", 1);
    
    while (*neigbors > 0)
        printf ("\t%d", *neigbors++);
    
    printf ("\n");
    
    printf ("\nRemoção da aresta (1, 6)\n");
    gma = gma_remove_arc (gma, 1, 6);
    
    printf ("\nMatriz de Adjacências, após remoção:\n");
    gma_print (gma);
    
    neigbors = gma_neighbors (gma, 1);
    printf ("\nVizinhos do vertice %d, após remoção\n", 1);
    
    while (*neigbors > 0)
        printf ("\t%d", *neigbors++);
    
    printf ("\n");
    
    printf ("\nBusca em profundidade:");
    gma_bep (gma);
    printf ("\n");
    
    gma_destroy (gma);
    
    printf ("\nFim!\n");
    
    return EXIT_SUCCESS;
}
