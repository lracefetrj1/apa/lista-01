//
// Created by lra on 04/11/22.
//

#include <stdio.h>
#include <stdlib.h>
#include "../include/L3GrafoLA.h"

int main (void)
{
    
    GLA *gla = gla_create (6);
    
    gla = gla_add_arc (gla, 2, 1);
    gla = gla_add_arc (gla, 1, 5);
    gla = gla_add_arc (gla, 1, 6);
    gla = gla_add_arc (gla, 5, 3);
    gla = gla_add_arc (gla, 5, 4);
    
    printf ("\nMatriz de Adjacências:\n");
    gla_print (gla);
    
    printf ("\nSão vizinhos (%d, %d): %s\n", 1, 3, gla_are_neighbors (gla, 1, 3) ? "Sim" : "Não");
    printf ("\nSão vizinhos (%d, %d): %s\n", 1, 5, gla_are_neighbors (gla, 1, 5) ? "Sim" : "Não");
    printf ("\nSão vizinhos (%d, %d): %s\n", 2, 1, gla_are_neighbors (gla, 2, 1) ? "Sim" : "Não");
    printf ("\nSão vizinhos (%d, %d): %s\n", 5, 5, gla_are_neighbors (gla, 5, 5) ? "Sim" : "Não");
    
    LTA *neigbors = gla_neighbors (gla, 1);
    printf ("\nVizinhos do vertice %d, antes da remoção\n", 1);
    
    while (neigbors)
    {
        printf ("\t%d", neigbors->vertice);
        neigbors = neigbors->next;
    }
    printf ("\n");
    
    printf ("\nRemoção da aresta (1, 6)\n");
    gla = gla_remove_arc (gla, 1, 5);
    
    printf ("\nMatriz de Adjacências, após remoção:\n");
    gla_print (gla);
    
    neigbors = gla_neighbors (gla, 1);
    printf ("\nVizinhos do vertice %d, após remoção\n", 1);
    
    while (neigbors)
    {
        printf ("\t%d", neigbors->vertice);
        neigbors = neigbors->next;
    }
    printf ("\n");
    
    printf ("\nBusca em profundidade:");
    gla_bep (gla);
    printf ("\n");
    
    gla_destroy (gla);
    
    printf ("\nFim!\n");
    
    return EXIT_SUCCESS;
}
