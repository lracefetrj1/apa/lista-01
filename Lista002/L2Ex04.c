//
// Created by lra on 16/10/22.
//
#include <stdio.h>
#include <stdlib.h>
#include "../include/Fila.h"

int main (int argc, char **argv)
{
    
    Fila *d = fla_criar ();
    
    fla_inserir (d, fla_criar_elemento (1, "AV01", "Rio", "Sampa"));
    fla_inserir (d, fla_criar_elemento (2, "AV02", "Sampa", "BH"));
    fla_inserir (d, fla_criar_elemento (3, "AV03", "BH", "Poa"));
    fla_inserir (d, fla_criar_elemento (4, "AV04", "Poa", "Rio"));
    
    printf ("\nQuantidade de aviões na fila: %d \n", fla_quantidade_elementos (d));
    
    printf ("\nFila de espera: \n");
    fla_fila_espera (d);
    
    printf ("\nAutorizar decolagem: \n");
    Elemento *e = fla_autorizar_decolagem (d);
    fla_imprimir_elemento (e);
    
    printf ("\nQuantidade de aviões na fila: %d \n", fla_quantidade_elementos (d));
    
    printf ("\nFila de espera: \n");
    fla_fila_espera (d);
    
    printf ("\nCaracterística do primeiro avião da fila: \n");
    fla_caracteristica_aviao (d);
    
    printf ("\nIncluir avião da fila: \n");
    e = fla_criar_elemento (5, "AV05", "Rio", "Sampa");
    fla_imprimir_elemento (e);
    fla_inserir (d, e);
    
    printf ("\nQuantidade de aviões na fila: %d \n", fla_quantidade_elementos (d));
    
    printf ("\nFila de espera: \n");
    fla_fila_espera (d);
    
    return EXIT_SUCCESS;
}