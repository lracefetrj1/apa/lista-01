//
// Created by lra on 16/10/22.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/Pilha.h"

int parentesis_balanceados (int *v, int n);

int main (int argc, char **argv)
{
    
    if (argc < 2)
    {
        printf ("Modo de usar: ./L2Ex02 <entrada>\n");
        printf ("Onde: entrada sequencia de caracteres '(' ou ')'.\n");
        printf ("Exemplo: ./L2Ex02 (((()))()(())())\n");
        return EXIT_FAILURE;
    }
    
    char *entrada = argv[1];
    
    int tam = strlen (entrada);
    int *vec = (int *) calloc (tam, sizeof (int));
    
    int d, j = 0;
    for (int i = 0; i < tam; i++)
    {
        d = *(entrada + i);
        
        if (d == '(')
            vec[j++] = d;
        else if (d == ')')
            vec[j++] = d;
    }
    
    printf ("Cadeia de caracteres:\n");
    for (int i = 0; i < j; i++)
    {
        printf ("%c ", vec[i]);
    }
    
    printf ("\n\n");
    
    int ret = parentesis_balanceados (vec, j);
    printf ("Cadeia balanceada? %s\n", ret ? "Sim" : "Não");
    
    free (vec);
    
    printf ("\nFim!\n");
    return EXIT_SUCCESS;
}

int parentesis_balanceados (int *v, int n)
{
    if (v == NULL || n < 2)
        return 0;
    
    int retorno = 1;
    
    Pilha *pilha = plh_criar ();
    
    int info;
    for (int i = 0; i < n; i++)
    {
        
        if (v[i] == '(')
            plh_empilhar (pilha, v[i]);
        else if (plh_desempilhar (pilha, &info) == 0)
        {
            retorno = 0;
            break;
        }
        
    }
    
    if (!plh_esta_vazia (pilha))
        retorno = 0;
    
    plh_destruir (pilha);
    
    return retorno;
}
