//
// Created by lra on 16/10/22.
//
#include <stdio.h>
#include <stdlib.h>
#include "../include/PilhaVec.h"

#define _MAXELEM 10
int main (int argc, char **argv)
{
    
    int tam = _MAXELEM;
    Elemento vec[_MAXELEM] = {};
    
    Pilha *p1 = vplh_criar (vec, tam);
    Pilha *p2 = vplh_criar (vec, tam);
    
    int info;
    
    vplh_empilhar (p1, 110);
    vplh_empilhar (p1, 120);
    vplh_empilhar (p1, 130);
    vplh_empilhar (p1, 110);
    vplh_empilhar (p1, 120);
    vplh_empilhar (p1, 130);
    
    vplh_empilhar (p2, 210);
    vplh_empilhar (p2, 220);
    vplh_empilhar (p2, 230);
    
    printf ("\nApós empilhar\n");
    for (int i = 0; i < tam; i++)
    {
        printf ("%d %d %d %d \n", i, vec[i].pos, vec[i].info, vec[i].uso);
    }
    
    vplh_desempilhar (p1, &info);
    
    printf ("\nApós desempilhar\n");
    for (int i = 0; i < tam; i++)
    {
        printf ("%d %d %d %d \n", i, vec[i].pos, vec[i].info, vec[i].uso);
    }
    
    vplh_empilhar (p2, 240);
    vplh_empilhar (p1, 140);
    
    printf ("\nApós empilhar\n");
    for (int i = 0; i < tam; i++)
    {
        printf ("%d %d %d %d \n", i, vec[i].pos, vec[i].info, vec[i].uso);
    }
    
    vplh_empilhar (p2, 250);
    vplh_empilhar (p1, 150);
    vplh_empilhar (p2, 260);
    vplh_empilhar (p1, 160);
    printf ("\nApós empilhar\n");
    
    for (int i = 0; i < tam; i++)
    {
        printf ("%d %d %d %d \n", i, vec[i].pos, vec[i].info, vec[i].uso);
    }
    
    for (int i = 0; i < tam; i++)
    {
        vplh_desempilhar (p1, &info);
        vplh_desempilhar (p2, &info);
    }
    
    printf ("\nApós desempilhar\n");
    for (int i = 0; i < tam; i++)
    {
        printf ("%d %d %d %d \n", i, vec[i].pos, vec[i].info, vec[i].uso);
    }
    
    vplh_destruir (p1);
    vplh_destruir (p2);
    
    printf ("\nApós destruir\n");
    for (int i = 0; i < tam; i++)
    {
        printf ("%d %d %d %d \n", i, vec[i].pos, vec[i].info, vec[i].uso);
    }
    
    printf ("\nFim!\n");
    
    return EXIT_SUCCESS;
}