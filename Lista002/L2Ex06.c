//
// Created by lra on 16/10/22.
//
#include <stdio.h>
#include <stdlib.h>
#include "../include/Arvore.h"

int main (int argc, char **argv)
{
    
    Arv *a = arv_criar ('a',
                        arv_criar ('b',
                            //      arv_criar_vazia (),
                            //      arv_criar_vazia ()
                                   arv_criar ('d', arv_criar_vazia (), arv_criar_vazia ()),
                                   arv_criar ('e', arv_criar_vazia (), arv_criar_vazia ())
                        ),
                        arv_criar ('c',
                                   arv_criar ('f', arv_criar_vazia (), arv_criar_vazia ()),
                                   arv_criar ('g', arv_criar_vazia (), arv_criar_vazia ())
                        )
    );
    
    Arv *b = arv_criar ('a',
                        arv_criar ('b',
                                   arv_criar ('d', arv_criar_vazia (), arv_criar_vazia ()),
                                   arv_criar ('e', arv_criar_vazia (), arv_criar_vazia ())
                        ),
                        arv_criar ('c',
                                   arv_criar ('f', arv_criar_vazia (), arv_criar_vazia ()),
                                   arv_criar ('g', arv_criar_vazia (), arv_criar_vazia ())
                        )
    );
    
    Arv *c = arv_criar ('a',
                        arv_criar ('b',
                                   arv_criar_vazia (),
                                   arv_criar_vazia ()
                        ),
                        arv_criar ('c',
                                   arv_criar ('f', arv_criar_vazia (), arv_criar_vazia ()),
                                   arv_criar ('g', arv_criar_vazia (), arv_criar_vazia ())
                        )
    );
    
    printf ("\nImprimir em notação textual (a):\n");
    arv_imprimir_nt (a);
    printf ("\n");
    
    printf ("\nImprimir em pré ordem (a):\n");
    arv_imprimir_pre (a);
    printf ("\n");
    
    printf ("\nEstritamente binária (a): %s \n", arv_estritamente_binaria (a) ? "Sim" : "Não");
    
    printf ("\nQuase Completa (a): %s \n", arv_quase_completa (a) ? "Sim" : "Não");
    
    printf ("\nCompleta (a): %s \n", arv_completa (a) ? "Sim" : "Não");
    
    printf ("\n");
    
    printf ("\nImprimir em notação textual (c):\n");
    arv_imprimir_nt (c);
    printf ("\n");
    
    printf ("\nImprimir em pré ordem (c):\n");
    arv_imprimir_pre (c);
    printf ("\n");
    
    printf ("\nEstritamente binária (c): %s \n", arv_estritamente_binaria (c) ? "Sim" : "Não");
    
    printf ("\nQuase Completa (c): %s \n", arv_quase_completa (c) ? "Sim" : "Não");
    
    printf ("\nCompleta (c): %s \n", arv_completa (c) ? "Sim" : "Não");
    
    printf ("\n");
    
    printf ("\nSimilar (NULL, NULL): %s \n", arv_similar (NULL, NULL) ? "Sim" : "Não");
    
    printf ("\nSimilar (a, NULL): %s \n", arv_similar (a, NULL) ? "Sim" : "Não");
    
    printf ("\nSimilar (NULL, a): %s \n", arv_similar (NULL, a) ? "Sim" : "Não");
    
    printf ("\nSimilar (a, a): %s \n", arv_similar (a, a) ? "Sim" : "Não");
    
    printf ("\nSimilar (a, b): %s \n", arv_similar (a, b) ? "Sim" : "Não");
    
    printf ("\nSimilar (b, a): %s \n", arv_similar (b, a) ? "Sim" : "Não");
    
    printf ("\nSimilar (a, c): %s \n", arv_similar (a, c) ? "Sim" : "Não");
    
    printf ("\nSimilar (c, a): %s \n", arv_similar (c, a) ? "Sim" : "Não");
    
    printf ("\nFim!\n");
    
    return EXIT_SUCCESS;
}