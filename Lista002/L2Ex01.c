#include <stdio.h>
#include <stdlib.h>
#include "../include/Lista.h"

int main (int argc, char **argv)
{
    
    if (argc < 2)
    {
        printf ("Modo de usar: ./L2Ex01 <n>\n");
        printf ("Onde: n número de elementos da lista (n > 0).\n");
        printf ("Exemplo: ./L2Ex01 20\n");
        return EXIT_FAILURE;
    }
    
    int n = atol (argv[1]);
    if (n < 1 || n > INT32_MAX)
    {
        printf ("n inválido (1 < N < %d).\n", INT32_MAX);
        return EXIT_FAILURE;
    }
    
    srand (43);
    
    List *lista = lst_create ();
    
    for (int i = 0; i < n; i++)
    {
        lista = lst_add (lista, rand ());
    }
    
    printf ("Lista original:\n");
    lst_print (lista);
    
    lista = lst_inverter (lista, n);
    
    printf ("\n");
    printf ("Lista invertida:\n");
    lst_print (lista);
    
    lista = lst_destroy (lista);
    
    printf ("\nFim!\n");
    return EXIT_SUCCESS;
}
